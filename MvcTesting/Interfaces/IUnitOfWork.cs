﻿using MvcTesting.Models;

namespace MvcTesting.Repositories
{
    public interface IUnitOfWork
    {
        GenericRepository<Product> ProductRepository { get; }

        void Dispose();
        void Save();
    }
}