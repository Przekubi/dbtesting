﻿using System.Data.Entity;
using MvcTesting.Models;

namespace MvcTesting.DAL
{
    public interface IContext
    {
        DbSet<Product> Products { get; set; }
        int SaveChanges();
        void Dispose();
    }
}