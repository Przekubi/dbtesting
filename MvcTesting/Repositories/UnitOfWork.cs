﻿using MvcTesting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTesting.Repositories
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private MvcTestingContext context = new MvcTestingContext();
        private GenericRepository<Product> productRepository;

        public GenericRepository<Product> ProductRepository
        {
            get
            {
                if(this.productRepository == null)
                {
                    this.productRepository = new GenericRepository<Product>(context);
                }
                return productRepository;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    context.Dispose();

                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}