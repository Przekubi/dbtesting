﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcMeshTest.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; }
        IManfucturerRepository Manfucturers { get; }
        int Save();
    }
}
