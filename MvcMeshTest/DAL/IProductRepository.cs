﻿using MvcMeshTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcMeshTest.DAL
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetProductsFromLargestQuantity(int count);
        IEnumerable<Product> GetProductWithManfucturer(int pageIndex, int pageSize);

    }
}
