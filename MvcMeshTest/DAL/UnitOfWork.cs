﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMeshTest.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private MvcMeshTestContext _context;

        public UnitOfWork()
        {
            _context = new MvcMeshTestContext();
            Products = new ProductRepository(_context);

        }

        public UnitOfWork(MvcMeshTestContext context)
        {
            _context = context;
            Products = new ProductRepository(context);
        }

        public IManfucturerRepository Manfucturers { get; private set; }

        public IProductRepository Products{ get;private set;}

        public int Save()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}