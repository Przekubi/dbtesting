﻿using MvcMeshTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace MvcMeshTest.DAL
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {

        public ProductRepository(MvcMeshTestContext context) : base(context)
        {
        }

        public IEnumerable<Product> GetProductWithManfucturer(int pageIndex, int pageSize)
        {
            return MvcMeshContext.Products.Include(o=>o.Manfucturer).OrderByDescending(c=>c.ProductName).Skip((pageIndex-1)*pageSize).Take(pageSize).ToList();
        }

        public IEnumerable<Product> GetProductsFromLargestQuantity(int count)
        {
            return MvcMeshContext.Products.OrderByDescending(c=>c.ProductQuantity).Take(count).ToList();
        }

        public MvcMeshTestContext MvcMeshContext
        {
            get { return Context as MvcMeshTestContext; }
        }
    }
}