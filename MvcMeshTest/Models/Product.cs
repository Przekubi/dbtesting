﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMeshTest.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int ProductQuantity { get; set; }

        public int ManfucturerID { get; set; }
        public virtual Manfucturer Manfucturer { get; set; }
        

    }
}