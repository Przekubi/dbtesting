﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMeshTest.Models
{
    public class Manfucturer
    {
        public int ManfucturerID { get; set; }
        public string ManfucturerName { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}