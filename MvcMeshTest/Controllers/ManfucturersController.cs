﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMeshTest.DAL;
using MvcMeshTest.Models;

namespace MvcMeshTest.Controllers
{
    public class ManfucturersController : Controller
    {
        private MvcMeshTestContext db = new MvcMeshTestContext();

        // GET: Manfucturers
        public ActionResult Index()
        {
            return View(db.Manfucturers.ToList());
        }

        // GET: Manfucturers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manfucturer manfucturer = db.Manfucturers.Find(id);
            if (manfucturer == null)
            {
                return HttpNotFound();
            }
            return View(manfucturer);
        }

        // GET: Manfucturers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manfucturers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ManfucturerID,ManfucturerName")] Manfucturer manfucturer)
        {
            if (ModelState.IsValid)
            {
                db.Manfucturers.Add(manfucturer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(manfucturer);
        }

        // GET: Manfucturers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manfucturer manfucturer = db.Manfucturers.Find(id);
            if (manfucturer == null)
            {
                return HttpNotFound();
            }
            return View(manfucturer);
        }

        // POST: Manfucturers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ManfucturerID,ManfucturerName")] Manfucturer manfucturer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manfucturer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(manfucturer);
        }

        // GET: Manfucturers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manfucturer manfucturer = db.Manfucturers.Find(id);
            if (manfucturer == null)
            {
                return HttpNotFound();
            }
            return View(manfucturer);
        }

        // POST: Manfucturers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Manfucturer manfucturer = db.Manfucturers.Find(id);
            db.Manfucturers.Remove(manfucturer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
