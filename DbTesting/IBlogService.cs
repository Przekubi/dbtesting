﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DbTesting
{
    public interface IBlogService
    {
        Blog AddBlog(string name, string url);
        List<Blog> GetAllBlogs();
        Task<List<Blog>> GetAllBlogsAsync();
    }
}