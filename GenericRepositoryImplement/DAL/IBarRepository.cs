﻿using GenericRepositoryImplement.Models;

namespace GenericRepositoryImplement.DAL
{
    public interface IBarRepository : IGenericRepository<Bar>
    {
        Bar GetSingle(int id);
    }
}