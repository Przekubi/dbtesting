﻿using GenericRepositoryImplement.Models;

namespace GenericRepositoryImplement.DAL
{
    public interface IItemRepository : IGenericRepository<Item>
    {
        Item GetSingle(int id);
    }
}