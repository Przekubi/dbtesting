﻿using GenericRepositoryImplement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericRepositoryImplement.DAL
{
    public class BarRepository : GenericRepository<GenericRepositoryImplementContext, Bar>, IBarRepository
    {
        public Bar GetSingle(int id)
        {
            var query = Context.Bars.FirstOrDefault(x => x.BarID == id);
            return query;
        }
    }
}