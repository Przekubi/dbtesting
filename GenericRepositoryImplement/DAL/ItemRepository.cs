﻿using GenericRepositoryImplement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericRepositoryImplement.DAL
{
    public class ItemRepository : GenericRepository<GenericRepositoryImplementContext, Item>, IItemRepository
    {
        public Item GetSingle(int id)
        {
            var query = GetAll().FirstOrDefault(x => x.ItemID == id);
            return query;
        }
    }
    
}