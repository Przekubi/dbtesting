﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericRepositoryImplement.Models
{
    public class Bar
    {
        public int BarID { get; set; }
        public string Name { get; set; }
    }
}