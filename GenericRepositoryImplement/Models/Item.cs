﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GenericRepositoryImplement.Models
{
    public class Item
    {
        [Key]
        public int ItemID { get; set; }
        public string Name { get; set; }
    }
}