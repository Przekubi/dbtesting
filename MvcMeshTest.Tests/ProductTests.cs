﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcMeshTest.DAL;
using MvcMeshTest.Controllers;
using System.Collections.Generic;
using MvcMeshTest.Models;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace MvcMeshTest.Tests
{
    [TestClass]
    public class ProductTests
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private Mock<IProductRepository> _mockRepository;
        List<Product> listProducts;
        Manfucturer manfucturer;
        ProductsController controller;
        [TestInitialize]
        public void Initalize()
        {
            
            _mockRepository = new Mock<IProductRepository>();
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            manfucturer = new Manfucturer()
            {
                ManfucturerName = "Tom",
                ManfucturerID = 1
            };
            listProducts = new List<Product>()
            {
                new Product() {ProductID =1, ProductName = "Zmywarka", Manfucturer=manfucturer, ManfucturerID=manfucturer.ManfucturerID },
                new Product() {ProductID=2, ProductName="Kosiarka" , Manfucturer=manfucturer, ManfucturerID=manfucturer.ManfucturerID}
            };
        }
        [Ignore]
        [TestMethod]
        public void ProductIndexTest()
        {
            _mockRepository.Setup(x => x.Get(It.IsAny<int>())).Returns((int i) => listProducts.Single(bo => bo.ProductID == i));
            _mockUnitOfWork.Setup(x => x.Products).Returns(_mockRepository.Object);
            var controller = new ProductsController();
            var result = controller.Index();
         }

       
    }
}
