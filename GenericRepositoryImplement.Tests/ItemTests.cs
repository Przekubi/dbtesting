﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GenericRepositoryImplement.DAL;
using Moq;
using GenericRepositoryImplement.Models;
using System.Linq;
using GenericRepositoryImplement.Controllers;
using System.Web.Mvc;

namespace GenericRepositoryImplement.Tests
{
    [TestClass]
    public class ItemTests
    {
        private IItemRepository itemRepo;
        [TestInitialize]
        public void Initialize()
        {
            Mock<IItemRepository> mock = new Mock<IItemRepository>();
            mock.Setup(m => m.GetAll()).Returns(new[]
            {
                new Item {ItemID=1, Name="Fake Name 1" },
                new Item {ItemID=2, Name="Fake Name 2" },
                new Item {ItemID=3, Name="Fake Name 3" }
            }.AsQueryable());
            mock.Setup(m => m.GetSingle(It.Is<int>(i =>
                  i == 1 || i == 2 || i == 3))).Returns<int>(
            r => new Item { ItemID = r, Name = string.Format("Fake Item {0}", r) });
            itemRepo = mock.Object;
        }

        [TestMethod]
        public void ItemController_Index_ReturnsModelTypeOfIqueryable()
        {
            //arange
            ItemsController controller = new ItemsController(itemRepo);
            //act
            var index = controller.Index() as ViewResult;
            var indexModel = index.Model;
            //assert
            Assert.IsInstanceOfType(indexModel, typeof(IQueryable<Item>));
           
        }
        [TestMethod]
        public void ItemController_Index_ReturnsIQueryableCountOf3()
        {
            //arrange
            ItemsController controller = new ItemsController(itemRepo);
            //act
            var index = controller.Index() as ViewResult;
            var indexModel = (IQueryable<object>)index.Model;
            //assert
            Assert.AreEqual<int>(3,indexModel.Count());
        }
        [TestMethod]
        public void ItemController_Details_ReturnsTypeOfViewResult()
        {
            //arrange
            ItemsController controller = new ItemsController(itemRepo);
            //act
            var detailsResult = controller.Details(1);
            //assert
            Assert.IsInstanceOfType(detailsResult, typeof(ViewResult));
        }

        [TestMethod]
        public void ItemControler_Details_ReturnTypeHttpNotFoundResult()
        {
            //arrange
            ItemsController controller = new ItemsController(itemRepo);
            //act
            var detailsResult = controller.Details(4);
            //assert
            Assert.IsInstanceOfType(detailsResult, typeof(HttpNotFoundResult));

        }

        [TestMethod]
        public void ItemController_Create_AddItemSuccess()
        {

            //arrange
            ItemsController controller = new ItemsController(itemRepo);
            //act
            var result = controller.Create(new Item { ItemID = 4, Name = "Fake Name 4" });

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));

        }

        [TestMethod]
        public void ItemController_Create_AddItemFailedSameObject()
        {
            //arrange
            ItemsController controller = new ItemsController(itemRepo);
            //act
            var result = controller.Create(new Item { ItemID = 3, Name = "Fake Name 3" });

            //assert
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));

        }
    }
}
